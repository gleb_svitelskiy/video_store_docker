# Dockerized development environment for video_store project

## INSTALLATION
1. Clone this repository
2. Navigate to repository's directory
3. Init submodules with command `git submodule init`
4. Update submodules with command `git submodule update`
5. If you don't have database data, you must init MySQL instance with command `docker-compose up mysql`
6. Wait until initialization process is finished. Notice `MySQL init process done. Ready for start up.` in consolle output. Then stop MySQL instance with Ctrl+C

## REQUIREMENTS
* Docker
* Docker Compose

## WHAT'S NEXT
Run (use -d to run containers in the background mode)

    docker-compose up
